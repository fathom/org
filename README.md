# README

Thanks for joining us here! 

[Fathom](https://fathom.network/) is building open and empowering educational infrastructure to change how the world learns. There's a lot to this vision, so there are many ways to contribute.

## Want to contribute?

If you want to contribute through code, documentation, or other formats, our [Contributing guide](CONTRIBUTING.md) is the best place to start. If you have questions, feel free to ask.

