# Contributing to fathom

Thank you for your interest in contributing to the fathom project!

We're excited that you're here, and hope your contribution experience is the right mix of engagement, ease, and enjoyment. Please note that it's a work in progress. We're trying to craft a process to work for all types of project contributions, be they code, documentation, or other formats or structures that support and extend what's possible in education and learning.

We welcome your participation, ideas and feedback.

### Tools
The basic tools we use to work are:
- [Gitlab](https://gitlab.com/fathom): to host and manage our work
- [Discord](https://discordapp.com/): to communicate with each other

<br>

#### Table of contents

1. [Issues](#issues)
    - [by Project](#by-project)
    - [by Issue Board](#by-issue-board)
    - [by Milestone](#by-milestone)
2. [Workflow](#workflow)
    - [1. Get an Idea](#1-get-an-idea)
    - [2. Create an Issue](#2-create-an-issue)
      - [Issue Templates](#issue-templates)
      - [Issue Boards](#issue-boards)
    - [3. Discuss and define the task](#3-discuss-and-define-the-task)
    - [4. Work on the issue](#4-work-on-the-issue)
    - [5. Review and merge the work](#5-review-and-merge-the-work)
    - [6. Retrospect on the work](#6-retrospect-on-the-work)
3. [Merge Requests](#merge-requests)

<br>

# Issues
[**Issues**](https://docs.gitlab.com/ee/user/project/issues/index.html) are the basic building blocks of our work. 
They capture tasks for our various projects. Think of them like todos, but much more powerful and collaborative.  

We organize our issues in three principal ways: by **project**, by **issue board**, and by **milestone:**

- ### by Project  
  Each issue pertains to a particular project.  
You can access all of a project's issues by going to its [**Issue Tracker**](https://docs.gitlab.com/ee/user/project/issues/#issue-tracker).  
There you can search, sort and filter them in various ways. 
Locate a project's issue tracker by going to the **Project folder > Issues.**

- ### by Issue Board
   [**Issue Boards**](https://docs.gitlab.com/ee/user/project/issue_board.html) 
are a tool for visually organizing and tracking work on a project's issues. 
Find a project's issue board by navigating to the **Project folder > Issues > Boards.**

- ### by Milestone
   [**Milestones**](https://docs.gitlab.com/ee/user/project/milestones/) 
capture goals and tasks we want to achieve within a certain timeframe. 
They represent points on our project roadmap, and can function like agile sprints. 
Once we've defined the objectives of a milestone and set its end date, we populate 
it with issues that will move us toward its completion.

   You can look at project milestones by going to the **Project folder > Issues > Milestones.**

<br>

# Workflow
Here's an overview of our workflow, from issue creation to task completion:   

   ```mermaid
   graph LR;
       A(Idea)-->B(Issue);
       B-->C(Discuss);
       C-->D(Do);
       D-->E(Review);
       E-->F(Complete);
       classDef workflow fill:#ccf,stroke:#333,stroke-width:1px;
       class A,B,C,D,E,F workflow;
   ```

<br>

<br>

- ### 1. Get an Idea:
  It all starts with an **idea**. There's something that needs doing.  
  This can be a new feature to build, a bug or error that needs fixing, some code or documents to refactor, a question to ask, or a topic to propose for discussion.

  It's best to first do a search of existing issues, to make sure what you're about to submit isn't a duplicate. And if you find an open issue that's pretty similar to your idea, read it over and join in on the discussion. But if this particular _thing_ you're thinking of hasn't yet been captured, it's time to turn it into an issue.

<br>

- ### 2. Create an Issue:
  [**Always start with an issue**](https://about.gitlab.com/2016/03/03/start-with-an-issue/). When creating an issue, keep in mind that it should be:
  - _small_: an issue should deal with a single topic
  - _descriptive_: anyone should be able to understand the issue 
  - _actionable_: we should be able to move forward with it

  **Here's how:**  
Navigate to the **Project folder > Issues.** Click the "New Issue" button, which will take you to the New Issue page. There you will be prompted to "Choose a template."

  ![Screen_Shot_2019-03-07_at_10.41.58_AM](/uploads/2222de5ceeac7d76b763ddabd2573b3d/Screen_Shot_2019-03-07_at_10.41.58_AM.png)   

---

  #### Issue Templates:
  Issue Templates are provided to help capture new issues.  
They're not mandatory, but they really help with our workflow.  
So we're hoping you'll find them both indispensable and delightful :smiley:

  - **Choose a template:** Select from available options:  
  ~Bug: to report errors in code or documentation.  
  ~Discussion: to submit an idea for discussion or research.  
  ~Feature: to propose something new to build or add on.  
  ~Question: to ask about something that's confusing or unclear (e.g., in codebase or docs).  
  ~Refactor: to propose something for edit, rewrite, or refactor (e.g., something in code or docs), to improve efficiency or clarity.


  - **Note:** If _none_ of the choices above is a good fit, there's also one called "Other".  

  - **Submit the issue:**  
After you've entered a "Title" and "Description" into the issue, you can leave the other fields blank (such as _Assignee_, _Milestone_, _Labels_, _Weight_ and _Due date_).

   Then click "Submit issue", and your newly-minted issue will be visible within that project.

---

  #### Issue Boards:
  A project's issue board is where we track and manage the progress of open issues.  
  
  ![Screen_Shot_2019-03-07_at_10.18.11_AM](/uploads/52a2396e21c5eaf9d4e3c0b62ad86a7f/Screen_Shot_2019-03-07_at_10.18.11_AM.png)
  
  This is the sequence of states an issue goes through as it moves from left to right, across the board:  
  Open --> ~discuss --> ~available --> ~in-Progress --> ~review --> ~revise --> Closed
  
  
  We'll describe each of them below.

---  

<br>

- ### 3. Discuss and define the task
  This beginning phase covers the prepping of the issue for work.

    - **Open**: A newly submitted issue will start out as "Open", which is the far left column on the board. It's like a backlog of issues. Project maintainers review new issues here, and work with their authors to make sure issues are ready to be moved into "Discuss".

    - ~discuss is an initial period in which other project members can join a discussion to understand the issue and evaluate it for implementation. Following this, the issue can be moved into "available".

    - ~available means the issue is now "available" for assignment. A contributor can take on an issue here and become its _assignee_. That means they'll work towards its completion and take accountability for its progress. And once it's been assigned, the issue is moved into the "in-Progress" column.

<br>

- ### 4. Work on the issue
  This middle phase is where the issue is being worked on by its assignee. Here, the issue can be in one of two states:
"in-Progress", which means it's moving forward; or "blocked" by something.

  - ~blocked represents a _state_ in which an issue's progress is halted. The reasons can be various: for example, it could be blocked by a separate but dependent issue; or by the discovery of a bug; or maybe the assignee is stuck and needs assistance. Here it's helpful for the assignee to add comments to describe how or why an issue is blocked, and to reach out to others who can provide ideas or support.

  - ~"in-Progress" just means that someone (i.e., the assignee) is working on the issue, implementing its plan and moving it forward. Once an assignee has completed and checked their work, it's ready for "review".

<br>

- ### 5. Review and merge the work
  This final phase is where the work is evaluated and completed. Project maintainers will review the work and help the assignee to ensure that the work is ready to be accepted, or _merged in_. The issue status will toggle between _review_ and _revise_ states as reviewers ask questions, and make suggestions, and as issue assignees respond with additions and revisions.

  NOTE: This phase introduces the concept of _merging in_ work, which we do via a Merge Request (MR). We will go over this in the next section.

  - ~review marks an issue that is ready to be reviewed. This means that someone _other than_ the issue assignee must evaluate the work done on the issue (and its corresponding MR), and write comments about any parts that need additional work or revision. The reviewer then moves the issue to the "revise" column so the issue assignee can follow up, as needed.

  - ~revise marks that an issue has been reviewed and needs additional work done, as per the reviewer's comments. It is now the issue assignee's turn to respond to these items and then resubmit the work for "review". 

  Note that there may be several rounds of "review" and "revise", with an issue toggling between these two states before it's finally approved for completion.

  Once the work has reached a satisfactory state, and no further revisions are needed, the reviewer will mark it complete by _merging_ the MR (i.e., the work) into the existing project. This act of merging in the MR also changes the issue status to "Closed" and moves it into that final column.

  **Closed**: Issues that are merged in are also automatically "Closed". But note that an issue can be marked "Closed" without being merged in. If an issue represents a piece of work that is no longer necessary (or that is duplicated elsewhere), the issue can be closed. So "Closed" really just means that an issue is no longer active.

<br>

- ### 6. Retrospect on the work
  Think about how the workflow, how it went, and note any parts of the process you can improve on with the next iteration. This might be a personal reflection. Or, if it's something systemic, consider opening an issue to address it :smiley:

<br>


# Merge Requests
Merge Requests (MRs) are how we introduce new work into our projects.
MRs function like Pull Requests (PRs) in github. Read more about them [here](https://docs.gitlab.com/ee/user/project/merge_requests/index.html#doc-nav).

You should create a MR if you want to make any changes to a fathom project. 

**Some MR guidelines:** 
- open them early so others can see your work
- clearly communicate your decisions and the motivations for any changes you're introducing
- scope them small, only do one thing in an MR
- never review your own MR (i.e., an MR that you open should always be assigned to someone else as the reviewer)

To open a Merge Request, navigate to **Project folder > Issues > Merge Requests.**  

We provide MR templates for the corresponding issue types.

And if it's your first MR -- or you just need some guidance -- don't worry, we'll help you through it.

--------------------------------------------------------------------------------

