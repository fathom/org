---
time-estimate: 
name: 
assignees: 
---

## Description
<!-- describe the project you're proposing -->

## Motivation
<!-- why is this important for fathom -->

## Timeline
<!-- how long will this take? be granular -->

## Deliverables
<!-- what concrete artifacts will this produce -->
